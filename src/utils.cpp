#include <spdlog/spdlog.h>

#include <chrono>
#include <ctime>
#include <filesystem>
#include <fmt/chrono.h>
#include <sstream>
#include <string>

std::string get_time_iso8601(void)
{
    const auto now = std::chrono::system_clock::now();
    return (fmt::format("{:%FT%T}", now));
}
