
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/files/42/Hackathon_campus/pucx/src/Interface.cpp" "src/CMakeFiles/pucx_lib.dir/Interface.cpp.o" "gcc" "src/CMakeFiles/pucx_lib.dir/Interface.cpp.o.d"
  "/files/42/Hackathon_campus/pucx/src/utils.cpp" "src/CMakeFiles/pucx_lib.dir/utils.cpp.o" "gcc" "src/CMakeFiles/pucx_lib.dir/utils.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/files/42/Hackathon_campus/pucx/extern/spdlog/CMakeFiles/spdlog.dir/DependInfo.cmake"
  "/files/42/Hackathon_campus/pucx/_deps/fmtlib-build/CMakeFiles/fmt.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
