#include <pucx/Interface.hpp>

#include <spdlog/spdlog.h>

#include <chrono>
#include <filesystem>
#include <fmt/chrono.h>
#include <fstream>
#include <iomanip>
#include <string>

namespace fs = std::filesystem;

Interface::Interface(const std::string resultPath)
    : _outputPath(resultPath)
{
    spdlog::trace("{Interface} Constructor called");

    fs::create_directories(_outputPath);
}

Interface::~Interface(void)
{
    spdlog::trace("{Interface} Destructor called");
}

void Interface::saveJson(const std::string fileName)
{
    fs::path file(this->_outputPath);

    file += "/";
    file.replace_filename(fileName);
    file.replace_extension(".json");

    // write JSON to file
    std::ofstream o(file.string());
    o << this->_jObject << std::endl;
    spdlog::info("{} Saved", file.string());
}

void Interface::savePrettyJson(const std::string fileName)
{
    fs::path file(this->_outputPath);

    file += "/";
    file.replace_filename(fileName);
    file.replace_extension(".json");

    // write prettified JSON to file
    std::ofstream o(file.string(), std::ios_base::app);
    o << std::setw(4) << this->_jObject << std::endl;
    spdlog::info("{} Saved", file.string());
}
