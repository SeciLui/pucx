#pragma once

#include <nlohmann/json.hpp>

#include <filesystem>
#include <string>

using json = nlohmann::json;

class Interface {
public:
    Interface(const std::string resultPath);
    ~Interface(void);

private:
    std::filesystem::path _outputPath;
    json _jObject;

public:
    void savePrettyJson(const std::string fileName);
    void saveJson(const std::string fileName);

public:
    template <typename T>
    void insertKey(const std::string key, T value)
    {
        this->_jObject[key] = value;
    };
    template <typename T>
    void insertKey(const std::string key1, const std::string key2, T value) { this->_jObject[key1][key2] = value; };
    template <typename T>
    void insertKey(const std::string key1, const std::string key2, const std::string key3, T value) { this->_jObject[key1][key2][key3] = value; };
};
