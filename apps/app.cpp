#include <pucx/Interface.hpp>
#include <pucx/utils.hpp>

#include <spdlog/cfg/env.h>
#include <spdlog/spdlog.h>

#include <fstream>
#include <iostream>
#include <numeric>
#include <unistd.h>
#include <vector>

std::vector<size_t> get_cpu_times()
{
    std::ifstream proc_stat("/proc/stat");
    proc_stat.ignore(5, ' '); // Skip the 'cpu' prefix.
    std::vector<size_t> times;
    for (size_t time; proc_stat >> time; times.push_back(time))
        ;

    return (times);
}

/* Parse the contents of /proc/meminfo (in buf), return value of "name"
 * (example: "MemTotal:")
 * Returns -errno if the entry cannot be found. */
long long get_entry(const char* name, const char* buf)
{
    const char* hit = strstr(buf, name);
    if (hit == NULL) {
        return -ENODATA;
    }

    errno = 0;
    long long val = strtoll(hit + strlen(name), NULL, 10);
    if (errno != 0) {
        int strtoll_errno = errno;
        spdlog::warn("{}: strtol() failed: {}", __func__, strerror(errno));
        return -strtoll_errno;
    }
    return val;
}
bool get_cpu_times(size_t& idle_time, size_t& total_time)
{
    const std::vector<size_t> cpu_times = get_cpu_times();
    if (cpu_times.size() < 4)
        return false;
    idle_time = cpu_times[3];
    total_time = std::accumulate(cpu_times.begin(), cpu_times.end(), 0);

    return (true);
}

int main(int argc, char* argv[])
{
    (void)argc;
    (void)argv;
    // Log levels can be loaded from argv/env using "SPDLOG_LEVEL"
    spdlog::cfg::load_env_levels();

    spdlog::info("App Begin");

    size_t previous_idle_time
        = 0,
        previous_total_time = 0;
    for (size_t idle_time, total_time; get_cpu_times(idle_time, total_time); sleep(1)) {
        const float idle_time_delta = idle_time - previous_idle_time;
        const float total_time_delta = total_time - previous_total_time;
        const float utilization = 100.0 * (1.0 - idle_time_delta / total_time_delta);

        Interface interface("./result");
        FILE* fd;
        fd = fopen("/proc/meminfo", "r");
        char buf[8192] = { 0 };

        fread(buf, 1, sizeof(buf) - 1, fd);
        size_t mem_total = get_entry("MemTotal:", buf);
        size_t mem_free = get_entry("MemFree:", buf);
        spdlog::info("CPU Usage: {}%", utilization);
        spdlog::info("MEM Total: {}kB", mem_total);
        spdlog::info("MEM Free: {}kB", mem_free);
        size_t mem_used = mem_total - mem_free;
        interface.insertKey("cpu usage (%)", utilization);
        interface.insertKey("mem used (kB)", mem_used);
        interface.savePrettyJson(get_time_iso8601());
        previous_idle_time
            = idle_time;
        previous_total_time = total_time;
        fclose(fd);
    }

    return (EXIT_SUCCESS);
}
